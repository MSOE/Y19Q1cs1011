package wk6;

import java.util.Scanner;

public class ComplexDriver {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        Complex c1 = getComplex(in);
        Complex c2 = getComplex(in);
        Complex answer = c1.times(c2);

        System.out.println(c1 + " * " + c2 + " = " + answer);
        System.out.println(c1 + " + " + c2 + " = " + c1.plus(c2));
        System.out.println(c1 + " - " + c2 + " = " + c1.minus(c2));
    }

    private static Complex getComplex(Scanner in) {
        System.out.println("Enter a complex number in the form: 3.0 + 4.3i");
        return new Complex(in.nextLine());
    }
}
