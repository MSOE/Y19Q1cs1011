/*
 * Course: CS1011
 * ...
 */
package wk6;

import java.util.Scanner;

/**
 * Complex class that represents complex objects of the form
 * real + imaginary
 *
 * @author taylor
 * @version 2018-10-18.1
 */
public class Complex {
    /**
     * The real component of the complex value
     */
    private double real;
    private double imag;

    private static boolean isPolar = false;

    /**
     * Constructs a complex object
     * @param value Represents an complex object in the form "3.8 + 2.1i"
     */
    public Complex(String value) {
        // Create a scanner object of what the user entered, but don't include
        // the "i" at the end
        Scanner parser = new Scanner(value.substring(0, value.length()-1));
        // Convert the real part to a double
        real = parser.nextDouble();
        String operator = parser.next();
        // Convert the imaginary part to a double
        imag = parser.nextDouble();
        if(operator.equals("-")) {
            imag = -imag;
        }
    }

    /**
     * Constructs a complex object
     * @param real the real component of the complex object
     * @param imag the imaginary component of the complex object
     */
    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public Complex(double real) {
        this(real, 0.0);
    }

    public static void setPolar() {
        isPolar = true;
    }

    public static void setCart() {
        isPolar = false;
    }

    /**
     * Calculates the product of two complex objects.
     *
     * Does not change the value of the calling object
     * @param that the value to be used in the product calculation
     * @return the result of multiplying the two complex objects together
     */
    public Complex times(Complex that) {
        return new Complex(this.real*that.real - this.imag*that.imag,
                this.real*that.imag + this.imag*that.real);
    }

    public Complex times(double real) {
        return new Complex(real*this.real, real*this.imag);
        //Complex that = new Complex(real);
        //return times(that);
    }

    public double getMagnitude() {
        return Math.sqrt(real*real + imag*imag);
    }

    public double getAngle() {
        return Math.toDegrees(Math.atan(imag/real));
    }

    /**
     * Creates a string representation of the object
     * @return The string representing the object
     */
    public String toString() {
        String result = "(";
        if(isPolar) {
            result += getMagnitude() + " | " + getAngle();
        } else {
            result += real;
            if (imag > 0) {
                result += " + " + imag + "i";
            } else if (imag < 0) {
                result += " - " + (-imag) + "i";
            }
        }
        return result + ")";
    }

    public boolean equals(Complex that) {
        return doubleEquivalent(real, that.real) && doubleEquivalent(imag, that.imag);
    }

    private static boolean doubleEquivalent(double a, double b) {
        return Math.abs(a-b) < 0.00001;
    }

    /**
     * Calculates the sum of two complex objects.
     *
     * Does not change the value of the calling object
     * @param that the value to be used in the sum calculation
     * @return the result of adding the two complex objects together
     */
    public Complex plus(Complex that) {
        return new Complex(this.real+that.real, this.imag+that.imag);
    }

    /**
     * Calculates the difference of two complex objects.
     *
     * Does not change the value of the calling object
     * @param that the value to be used in the difference calculation
     * @return the result of subtracting that from the calling object
     */
    public Complex minus(Complex that) {
        return new Complex(this.real-that.real, this.imag-that.imag);
    }

    public void swap(Complex that) {
        double temp = that.real;
        that.real = this.real;
        this.real = temp;
        temp = that.imag;
        that.imag = this.imag;
        this.imag = temp;
    }

    /**
     * @param a
     * @param b
     */
    public static void swap(Complex a, Complex b) {
        double tempReal = a.real;
        double tempImag = a.imag;
        a.real = b.real;
        a.imag  = b.imag;
        b.real = tempReal;
        b.imag  = tempImag;
    }
}





