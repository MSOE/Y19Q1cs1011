package wk6;

public class Student {
    private String name;
    private int id;
    private double gpa;
    public static final double PI = Math.PI;
    public static final int MINUTES_PER_HOUR = 60;

    public Student(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public boolean setGpa(double g) {
        boolean changed = false;
        if(g>=0 && g<=4) {
            gpa = g;
            changed = true;
        }
        return changed;
    }
}
