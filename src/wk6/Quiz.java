package wk6;

public class Quiz {
    public double quizA(double a, double b, double c) {
        return (-b-Math.sqrt(Math.abs(b*b - 4*a*c)))/(2*a);
    }

    public void quizB(String line) {
        int numUpper = 0;
        int numSpaces = 0;
        for(int i=0; i<line.length(); i+=1) {
            char character = line.charAt(i);
            if(Character.isUpperCase(character)) {
                numUpper += 1;
            }
            if(character==' ') {
                numSpaces += 1;
            }
        }
        System.out.println("Number of uppercase characters: " + numUpper);
        System.out.println("Number of spaces: " + numSpaces);
    }
}
