package wk3b;

import java.util.Scanner;

/*
Boolean operators:
* && - AND logic (binary), (true && true) ==> only case where answer is true
* || - OR logic (binary), (false || false) ==> only case where answer is false
* ! - NOT logic (unary), !true == false,  !false == true
* <, <=, >, >= Normal stuff
* == Equality
* != Inequality
*
* if(CONDITIONAL) {
*    // Statements to run if CONDITIONAL is true
* }
*
*
* if(CONDITIONAL) {
*    // Statements to run if CONDITIONAL is true
* } else {
*    // Statements to run if CONDITIONAL is false
* }
*
* switch (VARIABLE) {
*     case MATCH1:
*         // Statements to do if VARIABLE==MATCH1
*         break;
*     case MATCH2:
*         // Statements to do if VARIABLE==MATCH2
*         break;
*     case MATCH3:
*         // Statements to do if VARIABLE==MATCH3
*         break;
*     default:
*         // Statements to do if no match found
* }
*
* LOOPS
* while(CONDITIONAL) {
*    // Statements to run if CONDITIONAL is true
* }
*
* for(INITIALIZATION; CONDITIONAL; UPDATE) {
*    // Statements to run if CONDITIONAL is true
* }
*
* do {
*    // Statements that run once, an possibly more times
* } while(CONDITIONAL);
*/
public class Driver {
    public static void main(String[] args) {
        System.out.println("Please enter a menu choice (A, B, C, or D)");
        Scanner in = new Scanner(System.in);
        String input = in.next();
        if(input.equals(input.toUpperCase())) {
            System.out.println("You did not enter any lowercase letters");
        }
        input = input.toUpperCase();
        char choice = input.charAt(0);
        switch(choice) {
            case 'A':
                System.out.println("Menu option (A) is a fine choice, well done.");
                break;
            case 'B':
                System.out.println("Menu option (B) is an okay choice.");
                break;
            default:
                System.out.println("Enter a valid option.");

        }
    }

    public static void main2(String[] args) {
        {
            int i = 500;
            while (i <= 100) {
                System.out.println(i + "*" + i + " = " + i * i);
                i += 1;
            }
            System.out.println(i);
        }

        for(int i=1; i<=100; i+=1) {
            {
                System.out.println(i + "*" + i + " = " + i * i);
            }
        }

        int i=500;
        do {
            System.out.println(i + "*" + i + " = " + i*i);
            i+=1;
        } while(i<=100);
    }

}
