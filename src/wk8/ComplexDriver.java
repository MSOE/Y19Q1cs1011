package wk8;

import java.util.Scanner;
import wk6.Complex;

public class ComplexDriver {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        Complex c1 = new Complex(1.0);//getComplex(in);
        Complex c2 = new Complex(3.0, 3.0);//getComplex(in);
        System.out.println(c1);
        System.out.println(c2);
        Complex.setPolar();
        Complex.swap(c1, c2);
        System.out.println(c1);
        System.out.println(c2);
        c1.swap(c2);
        Complex.setCart();
        System.out.println(c1);
        System.out.println(c2);


    }

    private static Complex getComplex(Scanner in) {
        System.out.println("Enter a complex number in the form: 3.0 + 4.3i");
        return new Complex(in.nextLine());
    }
}
