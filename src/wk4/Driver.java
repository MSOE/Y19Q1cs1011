package wk4;

public class Driver {
    public static void main(String[] args) {
        double value = Math.PI;
        String pi = "" + value;
        String pi2 = Double.toString(value);
        String number = "8.333";
        double x = Double.parseDouble(number);
        System.out.format ("cos(90) = %.2f\n", Math.sin(Math.toRadians(90)));
        System.out.format ("approximately: %7.2f\n", Math.abs(-value));
        System.out.format ("approximately: %7d\n", Math.abs(0));
    }
}
