package wk9b;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListDriver {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        words.add("word");
        words.add("banana");
        words.add("banana");
        words.add("banana");
        System.out.println(words);
        if(!words.remove("bananas")) {
            System.out.println("did not remove anything");
        }
        words.add(1,null);
        System.out.println(words);
        System.out.println(words.get(0));
        //words.set(0, "words");
        System.out.println(words.indexOf(null));
        words.set(1, "bananas");
        System.out.println(words);
        System.out.println(words.lastIndexOf("banana"));

        ArrayList bad = new ArrayList();
        bad.add(3);
        bad.add("String");
        bad.add(new Scanner(System.in));
        System.out.println(bad);
    }
}
