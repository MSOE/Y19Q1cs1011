package wk9b;

import java.util.ArrayList;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        System.out.println("Enter your quiz scores. Then enter -1 when done.");
        Scanner in = new Scanner(System.in);
        int[] quizScores = getQuizScores(in);
        int lowScore = extractLow(quizScores);
        int sum = extractSum(quizScores);
        double quizGrade = 10.0*(sum-lowScore)/(quizScores.length-1);
        System.out.println(quizGrade);
        System.out.println(toString(quizScores));
    }

    private static int extractSum(ArrayList<Integer> quizScores) {
        int sum = 0;
        for(int i=0; i<quizScores.size(); i++) {
            sum += quizScores.get(i);
        }
        return sum;
    }

    private static int extractSum(int[] quizScores) {
        int sum = 0;
        for(int element : quizScores) {
            sum += element;
        }
        return sum;
    }

    private static int extractLow(ArrayList<Integer> quizScores) {
        int low = Integer.MAX_VALUE;
        for(int element : quizScores) {
            low = Math.min(low, element);
        }
        return low;
    }

    private static int extractLow(int[] quizScores) {
        int low = Integer.MAX_VALUE;
        for(int element : quizScores) {
            low = Math.min(low, element);
        }
        return low;
    }

    private static String toString(ArrayList<Integer> quizScores) {
        return quizScores.toString();
    }

    private static String toString(int[] quizScores) {
        String result = "[";
        for(int element : quizScores) {
            result += element + ", ";
        }
        result = result.substring(0, Math.max(1, result.length()-2)) + "]";
        return result;
    }

    private static ArrayList<Integer> getQuizScoresAL(Scanner in) {
        ArrayList<Integer> scores = new ArrayList<>();
        int score;
        do {
            if(!in.hasNextInt()) {
                System.out.println("Enter a score as an integer, -1 when done");
            }
            score = in.nextInt();
            if(score!=-1) {
                scores.add(score);
            }
        } while(score!=-1);
        return scores;
    }

    private static int[] getQuizScores(Scanner in) {
        int[] scores = new int[0];
        int score;
        do {
            if(!in.hasNextInt()) {
                System.out.println("Enter a score as an integer, -1 when done");
            }
            score = in.nextInt();
            if(score!=-1) {
                scores = addScore(scores, score);
            }
        } while(score!=-1);
        return scores;
    }

    private static int[] addScore(int[] scores, int score) {
        int[] biggerScores = new int[scores.length+1];
        for(int i=0; i<scores.length; i++) {
            biggerScores[i] = scores[i];
        }
        biggerScores[biggerScores.length-1] = score;
        scores = biggerScores;
        return scores;
    }
}
