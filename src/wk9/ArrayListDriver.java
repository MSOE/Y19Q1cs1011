package wk9;

import java.util.ArrayList;

public class ArrayListDriver {
    public static void main(String[] args) {
        String[] words2 = {"wow", "mom", "pig"};
        ArrayList<String> words = new ArrayList<>();
        words.add("wow");
        words.add("mom");
        words.add("pig");
        words.add("pig");
        words.add("pig");
        words.add("pig");
        words.add(0, "pig");
        words.set(3, "b!d");
        System.out.println(words);
        words.remove(2);
        words.remove("pig");
        System.out.println(words);
        System.out.println(words.indexOf("frank"));
        System.out.println(words.indexOf("pig"));
        System.out.println(words.lastIndexOf("pig"));
    }
}
