package wk9;

import java.util.ArrayList;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        System.out.println("Enter your quiz scores, finish with -1");
        Scanner in = new Scanner(System.in);
        ArrayList<Integer> quizScores = new ArrayList<>();
        quizScores.add(3);
        quizScores.add(8);
        System.out.println(quizScores);

    }
    public static void main2(String[] args) {
        System.out.println("Enter your quiz scores, finish with -1");
        Scanner in = new Scanner(System.in);
        int[] quizScores = getQuizScores(in);
        printArray(quizScores);
    }

    private static void printArray(ArrayList<Integer> quizScores) {
        System.out.println(quizScores);
    }

    private static void printArray(int[] quizScores) {
        String result = "[";
        for(int value : quizScores) {
            result += value + ", ";
        }
        result = result.substring(0, Math.max(1, result.length()-2)) + "]";
        System.out.println(result);
    }

    private static ArrayList<Integer> getQuizScoresAL(Scanner in) {
        ArrayList<Integer> scores = new ArrayList<>();
        int score;
        do {
            System.out.print("Enter a score (as an int): ");
            score = in.nextInt();
            if(score!=-1) {
                scores.add(score);
            }
        } while(score!=-1);
        return scores;
    }

    private static int[] getQuizScores(Scanner in) {
        int[] scores = new int[0];
        int score;
        do {
            System.out.print("Enter a score (as an int): ");
            score = in.nextInt();
            if(score!=-1) {
                scores = addScore(scores, score);
            }
        } while(score!=-1);
        return scores;
    }

    private static ArrayList<Integer> addScore(ArrayList<Integer>  scores, int score) {
        scores.add(score);
        return scores;
    }

    private static int[] addScore(int[] scores, int score) {
        int[] biggerScores = new int[scores.length+1];
        for(int i=0; i<scores.length; i++) {
            biggerScores[i] = scores[i];
        }
        biggerScores[biggerScores.length-1] = score;
        return biggerScores;
    }
}






