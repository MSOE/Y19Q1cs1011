package wk2a;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        // *=, /=, -=, +=, %=
        // x *= 3; <--> x = x * 3;
        String word = "381ae3";
        double q = Integer.parseInt(word, 16);
        System.out.println(q);
        double x = 8.11;
        Double y = x;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of pennies: ");
        String input = in.next();
        int pennies = Integer.parseInt(input);
        //int pennies = in.nextInt();
        int numQuarters = pennies/25;
        int penniesRemaining = pennies%25; // = pennies - numQuarters*25
        System.out.println(pennies + " pennies can be represented as "
        + numQuarters + " quarters and " + penniesRemaining + " pennies.");
    }



    public static void main2(String[] args) {
        int h = 8;
        int i = 3 + -h * 2;
        double x = 3.8;
        int j = (int)(x+0.5);
        j = (int)Math.round(x);
        Scanner in = new Scanner(System.in);
        String word = "word";
        System.out.println("Please enter a sentence now.");
        String sentence = in.nextLine();
        System.out.println("Please tell me the index of the character of interest.");
        int index = in.nextInt();
        int lengthOfSentence = sentence.length();
        int position = index + 1;
        System.out.println("You entered a sentence with " + lengthOfSentence + " characters in it.");
        System.out.println("The character at the " + position + "th position is: '" + sentence.charAt(index) + "'");
        double middleIndex = sentence.length()/(double)2;
        System.out.println("The middle character in your sentence is: '" + sentence.charAt((int)middleIndex) + "'.");
        System.out.println("Length: " + lengthOfSentence + " Middle index: " + middleIndex);
    }
}
