package wk2b;

import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // x *= 3; <--> x = x*3;
        System.out.print("Enter the number of pennies: ");
        String input = in.next();
        int cents = Integer.parseInt(input);
        int centsLeft = cents%25;
        int numQuarters = cents/25;
        System.out.println(cents + " pennies can be represented as " + -numQuarters
        + " quarters and " + centsLeft + " pennies.");
    }

    public static void main3(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a day of the week and a sentence that makes you smile.");
        String word = keyboard.nextLine();
        int middle = word.length()/2;
        System.out.println(middle);
        System.out.println(word.substring(middle));
    }

    public static void main2(String[] args) {
        final double POUNDS_PER_KG = 2.2;
        System.out.print("Enter your weight in pounds: ");
        Scanner keyboard = new Scanner(System.in);
        int weightInPounds = keyboard.nextInt();
        double weightInKg = weightInPounds / POUNDS_PER_KG;
        System.out.println("Your mass is: " + weightInKg + " Kg.");
    }
}
